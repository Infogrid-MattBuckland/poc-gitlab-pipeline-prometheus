# Poc Gitlab Pipeline Prometheus

```
export GITLAB_TOKEN=<your_token>
docker-compose up
```

Connect to `localhost:9090` in a browser. Observe `gitlab_ci_*` metrics.
